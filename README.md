# KamSol

**KamSol** is FEM-based simulator to extract the interface tractions from slip and off-interface strain measurements.

## License

Copyright &copy; 2022 ETH Zurich (David S. Kammer \& Mohit Pundir)

KamSol is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

KamSol is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along with KamSol.  If not, see <https://www.gnu.org/licenses/>.



## Getting started

**Requirements**:

- [Fenics](https://fenicsproject.org/) (version 2019.1.0)

Easy solution to get Fenics is to install [Anaconda](https://anaconda.org) following [these instructions](https://anaconda.org/conda-forge/fenics). Alternatively, KamSol can be run directly on google colab where Fenics installation is automatically handled. 


**Get KamSol**:

KamSol is available at: [KamSol](https://gitlab.ethz.ch/cmbm-public/toolboxes/kamsol).

Options:
1. *(easy)* by downloading it: [KamSol Downloads](https://gitlab.ethz.ch/cmbm-public/toolboxes/kamsol/-/releases)
2. *(advanced)* by cloning it: `git clone https://gitlab.ethz.ch/cmbm-public/toolboxes/kamsol.git`


**Run KamSol**:

You may run KamSol by following these steps

1. execute the Jupyter Notebook `KamSol.ipynb`


## Documentation

Detailed information can be found directly in the Jupyter Notebook `KamSol.ipynb`. Results of KamSol are saved in a matlab file (*e.g.* input data: `data.mat` -> output data: `data_interface_data.mat`)

